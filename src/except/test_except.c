/******************************************************************************
        (c) COPYRIGHT 2011-  by D.C.P  Co.,Ltd
                          All rights reserved.
@file: test_err.c
@brief: 主要是异常模块的函数进行测试，对模块内的ASSERT宏和EXCEPTION宏还有
        except_register_callback函数进行测试，测试，分为三个内容
        1）ASSERT宏测试：
@attention gcc  -rdynamic .././errno/err.c except.c test_except.c -o test -lpthread
@author   D.C.P 			                                                 
@version  0.0.0.1                                                               
@date     2017/03/11
******************************************************************************/
#include ".././util/util_inc.h"

int funcstacksum=0;
/**********************************************************************************************
*   Name：测试断言正常使用
**********************************************************************************************/
/**
* @brief 测试断言正常使用
*/
void _test_ASSERT(void)
{
    //测试断言宏的正常运行情况
    printf("assert wasn't happend\n");
    ASSERT(1);//不产生断言
    printf("assert was happend\n");
    ASSERT(0);//产生断言
    printf("ASSERT test end\n");
}
/**********************************************************************************************
*   Name：测试异常正常使用（没有注册函数）
**********************************************************************************************/
/**
* @brief 异常处理 没有注册程序
*/
void _test_EXCEPTION_none(void)
{
    //测试异常宏的正常运行情况
    printf("exception was happend\n");
    EXCEPTION(ERR_ENOSPC, "ERR_ENOSPC test!!!!");
    printf("exception test end\n");

}
/**********************************************************************************************
*   Name：测试异常正常使用（有注册函数）
**********************************************************************************************/
/**
* @brief 函数注册
*/
void register_func( void *arg)
{
    u32_t *err = (u32_t *)arg;
    printf("------------register_func%d: err=%d\n", ++funcstacksum, *err);
}


/**
* @brief 异常处理 过量注册程序
*/
void _test_EXCEPTION_over(void)
{
    //测试异常宏的正常运行情况
    printf("register function\n");
    except_register_callback( register_func, NULL);
    except_register_callback( register_func, NULL);
    except_register_callback( register_func, NULL);
    except_register_callback( register_func, NULL);
    except_register_callback( register_func, NULL);
    except_register_callback( register_func, NULL);
    except_register_callback( register_func, NULL);
    except_register_callback( register_func, NULL);
    except_register_callback( register_func, NULL);
    printf("register function finished\n");
    //测试异常宏的正常运行情况
    printf("exception was happend\n");
    EXCEPTION(ERR_ENOSPC, "ERR_ENOSPC test!!!!\n");
    printf("exception test end\n");

}
/**********************************************************************************************
*   Name：测试异常信号处理函数内正常使用
**********************************************************************************************/
/**
* @brief 测试信号产出的异常，信号处理函数
*/
void handler(int s)   
{  
    //测试异常宏的正常运行情况
    printf("exception was happend\n");
    EXCEPTION(ERR_ENOSPC, "ERR_ENOSPC test!!!!");
    printf("exception test end\n");
}   
/**
* @brief 测试信号产出的异常 测试函数
*/
void _test_EXCEPTION_signal(void)
{
    printf("register signal\n");
    signal(SIGSEGV,handler);//段错误信号
    printf("trigger signal\n");
    raise(SIGSEGV);
    printf("trigger signal success\n");
}
/**********************************************************************************************
*   Name：测试主函数
**********************************************************************************************/
/**
* @brief 一层层调用，测试函数堆栈backtrace函数
*/
void expect_test(void)
{
    //_test_ASSERT();//测试断言宏的正常使用
    //_test_EXCEPTION_none();//测试没有开启注册的异常宏的运行
    //_test_EXCEPTION_over();//测试过量注册的异常宏的运行
    _test_EXCEPTION_signal();//测试信号内的异常宏的运行
}


int main (void)
{
    expect_test();
    return 0;
}
