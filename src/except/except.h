/******************************************************************************
        (c) COPYRIGHT 2017-  by D.C.P  Co.,Ltd
                          All rights reserved.
@file: except.h
@brief:对于异常模块的结构体系的定义 接口宏的定义 公共函数的定义
@author   D.C.P 			                                                 
@version  0.0.0.1                                                               
@date     2017/03/02
-------------------------------------------------------------------------------
  Change History :                                                              
                 | <Date>      | <Version> | <Author> | <Description>                     
-------------------------------------------------------------------------------
  @todo          | 2017/03/06  | 0.0.0.1   | D.C.P    | 创建文件             
******************************************************************************/
#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */

#ifndef __INC_EXCEPT_H__
#define __INC_EXCEPT_H__

/**  @brief 成功返回 */
#define EXCEPT_SUCC FUNC_SUCC
/**  @brief 失败返回 */
#define EXCEPT_FAIL FUNC_FAIL
/**  @brief 失败返回空指针 */
#define EXCEPT_NULL FUNC_NULL

/**  @brief 获取存储空间 */
#define Except_malloc( size ) malloc( size )
/**  @brief 释放存储空间 */
#define Except_free( ptr ) free( ptr )

/**  @brief 异常处理指针 错误码参数*/
typedef void (*Handler_t)( void * );
/**  @brief 异常处理函数栈大小*/
#define FUNCSTACK 5
/**  @brief 退出调用函数栈*/
typedef struct {
    Handler_t       Func;         /**< 需要回调的函数 */
    void *          arg;          /**< 函数参数 */
}exitstack_t;

/**  @brief 退出函数栈 */
typedef struct {
    exitstack_t     ExitFunc[FUNCSTACK];    /**< 回调的退出函数数组 */
    u8_t            SP;                     /**< 函数指针 */
}ExitFuncStack;

/**  @brief 异常结构体 */
typedef struct {
        u32_t       Line;         /**< 出错位置所在的行数 */
        string_t    File;         /**< 出错位置所在的文件 */
        string_t    Function;     /**< 出错位置所在的函数 */
        err_t*      Reason;       /**< 出错的原因 */
        time_t      Time;         /**< 出错的时间 */
        char **     Stack;        /**< 栈的调用信息 内容使用完需要释放*/
        size_t      StackSize;    /**< 栈的调用消息大小*/
        string_t    Desc;         /**< 异常描述*/
}Exception;
/******************************************************************
*   Name：私有函数接口
*   Desc：内部调用的一些函数
******************************************************************/
/**
* @brief 异常处理函数
* @param [in] err 异常的错误码
* @param [in] file 异常所在文件
* @param [in] line 异常所在的行数
* @param [in] function 异常所在的函数
* @param [in] stcak 函数调用栈内容
* @param [in] stacksize 函数栈大小
* @param [in] desc 对于异常的描述
* @return 格式化完成的内容，否则是EXCEPT_NULL
*/
void _except_handler( u32_t err, const string_t file, const u32_t line, char const* function, 
                     char ** stack, size_t stacksize, string_t desc );
/**
* @brief 调用函数栈
* @return 无返回值
*/
void _except_exit_func_stack( u32_t err );
/******************************************************************
*   Name：私有接口宏
*   Desc：内部调用的一些宏，主要是获得异常函数调用堆栈
******************************************************************/
/**  
* @brief 获取函数堆栈信息宏 
* @param [in] stackinfo 堆栈信息 
* @param [in] size      堆栈大小
* @attention gcc编译时候需要加入-rdynamic才能打印函数名 否则只是函数地址 需要爱到add2line解析
*/
#define _EXCEPT_GET_FUNCSTACK( stackinfo , size)\
do{\
    void *stack[5];\
    size = backtrace (stack, 5);\
    stackinfo = backtrace_symbols (stack, size);\
}while(0)
/**  
* @brief 释放堆栈信息宏 
* @param [in] stackinfo 堆栈信息 
*/
#define _EXCEPT_DESTROY_FUNCSTACK( stackinfo )\
do{\
  Except_free( stackinfo );\
  stackinfo =NULL;\
}while(0)
/**  
* @brief 获得异常产生时间宏 
* @param [in] timep 异常产生时间
*/
#define _EXCEPT_GET_TIME( timep ) time(&timep)

/**  
* @brief 获得异常处理宏 
* @param [in] err       错误码
* @param [in] file      产生错误的文件
* @param [in] line      产生错误的行
* @param [in] function  产生错误的函数
* @param [in] desc      错误描述
*/
#define _EXCEPT_HANDLER( err, file, line, function, desc)\
do{\
    char ** stackinfo;\
    size_t  stacksize =0;\
    _EXCEPT_GET_FUNCSTACK( stackinfo, stacksize );\
    _except_handler(err, file, line, function, stackinfo, stacksize, desc);\
    _EXCEPT_DESTROY_FUNCSTACK( stackinfo );\
}while(0)

/**  
* @brief 断言宏 
* @param [in] Except 需判断的异常 
* @attention 当定义了NODEBUG就将debug模式转成release模式
*/
#ifdef NDEBUG 
#define _ASSERT( Except )     ((void)0)
#else
#define _ASSERT( Except )\
do{\
    if( !Except ) {\
        _EXCEPT_HANDLER( ERR_ASSERT, __FILE__, __LINE__, __FUNCTION__, NULL );\
        while(1);\
    }\
}while(0)
#endif
/**  
* @brief 处理异常的宏 
* @param [in] err       错误码
* @param [in] desc      错误描述
*/
#define _EXCEPTION( Err, Decs )\
do{\
    _EXCEPT_HANDLER( Err, __FILE__, __LINE__, __FUNCTION__, Decs );\
    _except_exit_func_stack(Err);\
}while(0)

#endif/*__INC_EXCEPT_H__*/

#ifdef __cplusplus
}
#endif /* __cplusplus */
