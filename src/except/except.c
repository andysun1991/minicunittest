/******************************************************************************
        (c) COPYRIGHT 2017-  by D.C.P  Co.,Ltd
                          All rights reserved.
@file: except.c
@brief: 本模块主要是对于异常的捕获以及处理，其内部逻辑结构主要是分成两个部分，
        首先是异常现场保存处理，将异常发生的文件，函数，行数记录，并记录函数调用栈和发生
        时间，发生原因记录然后打印并写入核心转储文件。
        其次是如果是断言不这步处理而是进入无线循环，如果是异常则longjmp到异常处理部分，
        进行程序退出前的处理，调用异常函数栈的函数处理后调用默认处理函数退出程序
        异常模块主要是有两个公共接口宏和一个接口函数组成。
        except_register_callback：这个函数注册回调函数，当需要在推出前有其他操作类似
        内存释放，内容保存等就需要调用这个函数注册到异常里面，当退出程序是调用
        ASSERT( Exception ) ：断言处理宏，当需要进入debug模式断言时候可以调用，如果程序
        进入release模式，跟c库方式一样定义#define NDEBUG就可以去除assert影响。
        EXCEPTION( Err, Decs )：异常处理宏，当程序发生不可逆错误时候，调用整个宏进行异常
        记录保存，并正常退出，例如段错误。
@attention： 本模块关联外部文件和模块 util/utildef.h,utilinc.h err模块
@author   D.C.P 			                                                 
@version  0.0.0.1                                                               
@date     2017/03/02
-------------------------------------------------------------------------------
  Change History :                                                              
                 | <Date>      | <Version> | <Author> | <Description>                     
-------------------------------------------------------------------------------
  @todo          | 2017/03/07  | 0.0.0.1   | D.C.P    | 创建文件            
******************************************************************************/
#include ".././util/util_inc.h"
#include "except.h"

/**  @brief 异常处理函数栈 */
static ExitFuncStack MgmtStack ={0,};

/**  @brief 核心转储文件 */
#define EXCEPT_FILE "/tmp/coredump.txt"

/**
* @brief 将内容写入到核心转储文件		 
* @retval FUNC_SUCC  表示成功
* @retval FUNC_FAIL  表示失败
*/
static u32_t 
_except_write_coredump( const string_t excepttext )
{
    FILE    *fp =NULL;

    fp=fopen( EXCEPT_FILE ,"a+" );
    if(NULL == fp)
    {
        return EXCEPT_FAIL;//错误返回
    }
    fwrite( excepttext, 1, strlen(excepttext), fp); /* 写入文件*/

    fclose(fp);
    fp=NULL;//需要指向空，否则会指向原打开文件地址
    
     return EXCEPT_SUCC;
}



/**
* @brief 初始化一个异常函数
* @return 无返回值
*/
static void 
_except_init_package( Exception * package )
{
    //初始化异常包
    package->Line       =0;//异常所在行
    package->File       =NULL;//异常所在文件
    package->Function   =NULL;//异常所在函数
    package->Time       =0;//异常时间
    package->Reason     =NULL;//异常原因
    package->Stack      =NULL;//栈
    package->StackSize  =0;//栈大小
    package->Desc       =NULL;//异常描述
}

/**
* @brief 异常内容格式化函数
* @param [in] buf 存储异常缓冲区
* @param [in] except 需要格式成字符串的异常
* @retval FUNC_SUCC  表示成功
* @retval FUNC_FAIL  表示失败
*/
static u32_t 
_except_format( string_t buf, u32_t buflen, const Exception except )
{
    if( NULL == buf )
    {
        _except_write_coredump("_except_format: the buf or except was NULL buf是空的没法保存文件");
        return EXCEPT_FAIL;
    }

    string_t format = buf;//格式化
    size_t size = 0, formatsize =buflen;//format缓冲区填充数据长度
    u8_t    i;//堆栈打印
    u32_t  errCode;

    size += snprintf( format, formatsize, "==================================Exception====================================\n");
    //异常时间
    if( 0 != except.Time )
            size += snprintf( format+size, formatsize-size, "%s \n", ctime(&except.Time) );
    //异常原因
    if( NULL != except.Reason )
    {
            errCode = err_get_errno(except.Reason);
            size += snprintf( format+size, formatsize-size, "Errno %d: %s \n\r", errCode, err_strerr(errCode) );
    }
    //异常描述
    if( NULL != except.Desc )
            size += snprintf( format+size, formatsize-size, "User-defined Description:%s \n\n", except.Desc );
    //异常位置
    if( (NULL != except.File) && (0 != except.Line) && (0 != except.Function) )
            size += snprintf( format+size, formatsize-size, "File name:%s, Line:%d Function:%s\n", except.File, except.Line, except.Function );
    //异常函数堆栈调用
    if( NULL != except.Stack )
            for (i = except.StackSize-1; i >0 ; i--)  size += snprintf( format+size, formatsize-size, "Function Stack: %s \n", except.Stack[i] );

    return EXCEPT_SUCC;
}

/**
* @brief 异常处理函数
* @param [in] err 异常的错误码
* @param [in] file 异常所在文件
* @param [in] line 异常所在的行数
* @param [in] function 异常所在的函数
* @param [in] stcak 函数调用栈内容
* @param [in] stacksize 函数栈大小
* @param [in] desc 对于异常的描述
* @return 无返回值
*/
void 
_except_handler(u32_t err, const string_t file, const u32_t line, char const* function, char ** stack, size_t stacksize, string_t desc )
{
    Exception except;
    char format[1024];//获得格式后完数据

    //初始化异常
    _except_init_package(&except);

    except.Line       =line;//异常所在行
    except.File       =file;//异常所在文件
    except.Function   =(char *)function;//异常所在函数
    except.Reason     =err_get_info(err);//异常原因
    except.Stack      =stack;//栈
    except.StackSize  =stacksize;//栈大小
    except.Desc       =desc;//异常描述
    _EXCEPT_GET_TIME( except.Time );//异常时间

    if( EXCEPT_FAIL !=  _except_format( format, 1024, except ) ) 
    {
        printf("%s \n", format);//打印异常
        _except_write_coredump(format);//保存异常到核心转储文件
    }

}

/**
* @brief 默认回调函数
* @param [in] err 错误码
* @return 无返回值
*/
static void 
_except_default_callback_exit( void *arg )
{
    u32_t *err = (u32_t*)arg;
    exit(*err);//安全退出程序，并返回错误码
}

/**
* @brief 调用函数栈
* @return 无返回值
*/
void 
_except_exit_func_stack( u32_t err )
{
    u8_t TmpSP =MgmtStack.SP;

    for( ; TmpSP>0 ; TmpSP-- )
        MgmtStack.ExitFunc[TmpSP-1].Func( &(MgmtStack.ExitFunc[TmpSP].arg) );//出栈

    _except_default_callback_exit(&err);//调用安全退出函数
}

/**
* @brief 注册回调函数
* @param [in] ExceptFunc 异常处理回调函数:void(*Handler_t)(void*)
* @retval FUNC_SUCC  表示成功
* @retval FUNC_FAIL  表示失败
*/
u32_t 
except_register_callback( Handler_t ExceptFunc, void *arg )
{

    if( NULL == ExceptFunc ) RETURN_ERR( ERR_PARA ,EXCEPT_FAIL);

    if( MgmtStack.SP < FUNCSTACK )//注册回调函数
    {
      MgmtStack.ExitFunc[MgmtStack.SP].Func= ExceptFunc;//回调函数
      MgmtStack.ExitFunc[MgmtStack.SP].arg = arg;//参数
      MgmtStack.SP++;
    }
    return EXCEPT_SUCC;
}
